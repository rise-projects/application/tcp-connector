# Unity TCP-Connector

This repository is an adaptation of the original [Unity Robotics Hub](https://github.com/Unity-Technologies/Unity-Robotics-Hub/blob/master/tutorials/ros_unity_integration/README.md) and [ROS TCP Connector](https://github.com/Unity-Technologies/ROS-TCP-Connector) repository, tailored to address specific custom use cases for RISE.

The TCP Connector serves as a bridge between Unity application and [ROS](https://www.ros.org/). It enables communication and data exchange between these two systems.<br>
For more information, please refer to the official [RISE documentation](https://rise-projects.pages.ub.uni-bielefeld.de/rise-documentation/contents.html).

<img src="https://gitlab.ub.uni-bielefeld.de/rise-projects/rise-documentation/-/raw/development/source/images/ros_serverendpoint.png?ref_type=heads" width="600" />

## How-to start the component standalone

* catkin_make
* source devel/setup.bash
* rosparam set /ROS_IP $ROS_IP
* rosparam set /ROS_TCP_PORT $ROS_TCP_PORT
* rosrun ros_tcp_endpoint ros-unity-endpoint.py