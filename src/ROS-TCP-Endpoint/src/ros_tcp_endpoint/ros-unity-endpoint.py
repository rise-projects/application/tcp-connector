#!/usr/bin/env python

"""ROS-TCP Server Endpoint

Main function of server endpoint to subscribe all relevant messages.
This server creates the connection between the unity application and the ROS environment.
It pipes the sended ros messages from one component via TCP to the other application.

This file has the following functions:

    * main - the main function of the script and start the server endpoint with all named topics.
"""

import rospy
import sys

from ros_tcp_endpoint import TcpServer
from std_msgs.msg import String

def main(connection_port):
    ros_node_name = rospy.get_param("/TCP_NODE_NAME", 'TCPServer')
    buffer_size = rospy.get_param("/TCP_BUFFER_SIZE", 1024)
    connections = rospy.get_param("/TCP_CONNECTIONS", 10)
    tcp_port = connection_port

    tcp_server = TcpServer(ros_node_name, buffer_size, connections, tcp_port=tcp_port)

    # Start the Server Endpoint
    rospy.init_node(ros_node_name, anonymous=True)

    tcp_server.start()
    rospy.spin()


if __name__ == "__main__":
    if(len(sys.argv) > 1):
    	port = sys.argv[1]
    	main(int(port))
    else:
    	print("Pls add a connection port number to the command line arguments")
