cmake_minimum_required(VERSION 2.8.3)
project(ros_tcp_endpoint)

find_package(catkin REQUIRED COMPONENTS
  rospy
  std_msgs
  message_generation
  actionlib_msgs
)

catkin_python_setup()

#add_action_files(DIRECTORY action) 

add_message_files(DIRECTORY msg)

add_service_files(DIRECTORY srv)

generate_messages(DEPENDENCIES std_msgs actionlib_msgs)

catkin_package(CATKIN_DEPENDS message_runtime actionlib_msgs)
